#include "Arduino.h"
#include "TimerOne.h"
#include "config.h"
#include "logicBoard.h"

int ledPin[] = {LED_LEFT_PIN, LED_MIDDLE_PIN, LED_RIGHT_PIN};

volatile boolean gameStarted;
volatile boolean animation;
volatile boolean endGameFlag;
volatile int directionPong;
volatile int countBlinky;
volatile int shoots;
volatile long reactTime;
boolean winnerFlagState;
boolean initMiddleGreen;
int gameSpeed;
int positionPong;
long millisTimerStarted;

void initLedBoard() {
  // led initialize and button intettupt
  for(int i = 0; i < 3; i++)
    pinMode(ledPin[i], OUTPUT);
  pinMode(LED_RED_PIN, OUTPUT);

  // normal variables
  positionPong = 1;
  shoots = 0;
  countBlinky = 8;
  gameSpeed = GAME_SPEED;
  reactTime = REACT_TIME;
  gameStarted = false;
  winnerFlagState = false;
  endGameFlag = false;
  initMiddleGreen = true;
}

void setStateGame(boolean state) {
  gameStarted = state;
  if (gameStarted) {
    // pot
    int speedChange = analogRead(POT_PIN);
    gameSpeed /= map(speedChange, 0, 1023, 1, 3);
    // randomSeed
    directionPong = (millis() % 2) == 0 ? -1 : 1;
    // initial animation
    animation = true;
  }
}

boolean getStateGame() {
  return gameStarted;
}

void pulseLed() {
  int fadeAmount = FADE_AMOUNT;
  int currIntensity = 0;
  
  int pressed = digitalRead(BUT_START_PIN);
  while (pressed == LOW) {
    analogWrite(LED_RED_PIN, currIntensity);
    currIntensity = currIntensity + fadeAmount;
    if (currIntensity == 0 || currIntensity == 255)
        fadeAmount = -fadeAmount ;
    pressed = digitalRead(BUT_START_PIN);
    delay(20);    
  }
  analogWrite(LED_RED_PIN, 0);
  attachInterrupt(digitalPinToInterrupt(BUT_LEFT_PIN), leftButtonPressed, RISING); 
  attachInterrupt(digitalPinToInterrupt(BUT_RIGHT_PIN), rightButtonPressed, RISING);
}

void animate() {
  if (animation) {
    positionPong = 1;
    // middle led
    digitalWrite(ledPin[1], HIGH);
    if(initMiddleGreen) {
      delay(1000);
      initMiddleGreen = false;
    } else {
      delay(gameSpeed);
    }
    digitalWrite(ledPin[1], LOW);
    // left or right led
    positionPong = directionPong == 1 ? 2 : 0;
    digitalWrite(ledPin[positionPong], HIGH);
    // setup timer with reactTime
    millisTimerStarted = millis();
    setupTimer();
    // change animation state, the pong hasn't to move from the current position
    animation = false;
  }
}

void setupTimer() {
  Timer1.initialize(reactTime * 1000);
  Timer1.attachInterrupt(endGame);
}

void rightButtonPressed() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  // example and complete code of the snippet here: https://learn.adafruit.com/multi-tasking-the-arduino-part-1/using-millis-for-timing
  if (interrupt_time - last_interrupt_time > 200) {
    actionsButtonPressed(2);
  }
  last_interrupt_time = interrupt_time;
}

void leftButtonPressed() {
  static unsigned long last_interrupt_time = 0;
  unsigned long interrupt_time = millis();
  // If interrupts come faster than 200ms, assume it's a bounce and ignore
  // example and complete code of the snippet here: https://learn.adafruit.com/multi-tasking-the-arduino-part-1/using-millis-for-timing
  if (interrupt_time - last_interrupt_time > 200) {
    actionsButtonPressed(0);
  }
  last_interrupt_time = interrupt_time;
}

void actionsButtonPressed(int buttonPressed) {
  noInterrupts();
  int internal_positionPong = positionPong;
  interrupts();
  if (internal_positionPong == buttonPressed) {
    noInterrupts();
    // increment counter of shoots
    shoots += 1;
    // turn off the led corrisponding to the button pressed
    digitalWrite(ledPin[positionPong], LOW);
    // update direction of the pong, the opposite
    directionPong = -directionPong;
    // update reactTime
    reactTime -= reactTime * (1.0 / 8.0);
    // set animation true, now the pong can move itself
    animation = true;
    // remove interrupt
    Timer1.detachInterrupt();
    interrupts();
  } else {
    // the button pressed is the wrong one so the game ends
    // we have to reset reactTime to zero, swipe positionPong to the winner player and call endGame function
    reactTime = 0;
    endGame();
  }
}

void endGame() {
  // check in order to avoid starter bug (look at the documentation for the explanation)
  boolean internal_endGameFlag = endGameFlag;
  if( millis() - millisTimerStarted > reactTime && !internal_endGameFlag) {
    noInterrupts();
    endGameFlag = true;
    // remove interrupt
    Timer1.detachInterrupt();
    // remove interrupt from buttons
    detachInterrupt(digitalPinToInterrupt(BUT_LEFT_PIN)); 
    detachInterrupt(digitalPinToInterrupt(BUT_RIGHT_PIN));
    // turn off all the leds
    for(int i = 0; i < 3; i++) {
      digitalWrite(ledPin[i], LOW);
    }
    interrupts();
    // blink the winner led
    Timer1.initialize(2000000 / countBlinky);
    Timer1.attachInterrupt(blinky);
    String finalPrint = "Game Over - The Winner is Player " + (String)(positionPong == 0 ? "RIGHT" : "LEFT") + " after " + shoots + " shots.";
    Serial.println(finalPrint);
  }
}

void blinky() {
  if(countBlinky > 0) {
    if(!winnerFlagState) {
      digitalWrite(ledPin[positionPong == 0 ? 2 : 0], HIGH);
    } else {
      digitalWrite(ledPin[positionPong == 0 ? 2 : 0], LOW);
    }
    countBlinky--;
    winnerFlagState = !winnerFlagState;          
  } else {
    // set the state game to false, the game is ended and the players can play a new game
    setStateGame(false);
    initMiddleGreen = true;
    endGameFlag = false;
    Timer1.detachInterrupt();
  }
}
