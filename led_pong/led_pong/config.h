#ifndef __CONFIG__
#define __CONFIG__

#define LED_RIGHT_PIN 6
#define LED_MIDDLE_PIN 7
#define LED_LEFT_PIN 8
#define LED_RED_PIN 5

#define BUT_RIGHT_PIN 3
#define BUT_LEFT_PIN 2
#define BUT_START_PIN 4

#define POT_PIN A0

#define FADE_AMOUNT 5
#define GAME_SPEED 1000
#define REACT_TIME 2000

#endif
