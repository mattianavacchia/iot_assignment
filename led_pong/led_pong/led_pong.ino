/*
 * SEIOT a.a. 2018/2019
 *
 * CONSEGNA #1 - LED PONG
 *
 * Hnatyshyn Nazar, Navacchia Mattia
 */
#include "config.h"
#include "logicBoard.h"

void setup(){
  Serial.begin(9600);
}

void loop(){
  Serial.println("Welcome to Led Pong. Press Key T3 to Start");
  initLedBoard();
  
  // Led pulse iniziale
  pulseLed();

  Serial.println("GO!");
  setStateGame(true);
  while(getStateGame()) {
    animate();
  }
}
