#ifndef __LEDBOARD__
#define __LEDBOARD__

void initLedBoard();
void pulseLed();
void setStateGame(boolean state);
boolean getStateGame();
void animate();
void setupTimer();
void rightButtonPressed();
void leftButtonPressed();
void actionsButtonPressed(int buttonPressed);
void endGame();
void blinky();

#endif
