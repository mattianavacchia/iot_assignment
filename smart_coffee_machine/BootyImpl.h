#ifndef __BOOTYIMPL__
#define __BOOTYIMPL__

#include "Booty.h"

class BootyImpl: public Booty {

public: 
  BootyImpl(int pin);
  int getValue();
  bool isNewValue();
  void resetNewValue();

private:
  int pin;
  int value;
};

#endif
