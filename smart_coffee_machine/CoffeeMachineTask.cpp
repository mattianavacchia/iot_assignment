#include "CoffeeMachineTask.h"
#include "ButtonImpl.h"
#include "BootyImpl.h"
#include "config.h"
#include "MsgService.h"

CoffeeMachineTask::CoffeeMachineTask(Person* p) {
  this->person = p;
  this->buttonMake = new ButtonImpl(BUTTON_MAKE);
  this->sugarBooty = new BootyImpl(POT_SUGAR);
  this->led[0] = new Led(LED_BLUE);
  this->led[1] = new Led(LED_YELLOW);
  this->led[2] = new Led(LED_GREEN);
}

void CoffeeMachineTask::init(int period) {
  Task::init(period);
  state = IDLE;
}

void CoffeeMachineTask::tick() {
  switch (state) {
    case IDLE: {
      if(!person->checkCanTakeCoffee() && person->getCoffeeNumber() == 0) {
        person->setMaintenance(true);
        state = MAINTENANCE;
        MsgService.sendMsg(MSG_CLEAR);
        delay(50);
        MsgService.sendMsg(MSG_NO_COFFEE);
      }
      if (person->checkWantCoffee()) {
        if (sugarBooty->isNewValue()) {
          String stringOne = MSG_SUGAR;
          String stringThree = stringOne + sugarBooty->getValue();
          MsgService.sendMsg(stringThree);
        }
        if (buttonMake->isPressed()) {
          state = DOCOFFEE;
          person->setWantCoffee(false);
        }
      } else {
        sugarBooty->resetNewValue();
      }
      break;
    }
    case DOCOFFEE: {
      person->decreaseCoffeeNumber();
      //Serial.println("Making a coffee");
      MsgService.sendMsg(MSG_MAKING_COFFEE);
      for(int i = 0; i < 3; i++) {
        led[i]->switchOn();
        delay(1000);
        led[i]->switchOff();
      }
      //Serial.println("The coffee is ready");
      MsgService.sendMsg(MSG_COFFEE_READY);
      state = COFFEEREADY;
      break;
    }
    case COFFEEREADY: {
      person->setCanTakeCoffee(true);
      state = IDLE;
      break;
    }
    case MAINTENANCE: {
      if (MsgService.isMsgAvailable()) {
        Msg* msg = MsgService.receiveMsg();
        if (msg->getContent() == MSG_REFILL_COFFEE) {
          person->refillCoffee();
          String stringOne = MSG_REFILL_DONE;
          String stringThree = stringOne + NUM_COFFEE;
          MsgService.sendMsg(stringThree);
          state = IDLE;
          person->setMaintenance(false);
          sugarBooty->resetNewValue();
        }
      }
      break;
    }
  }
}
