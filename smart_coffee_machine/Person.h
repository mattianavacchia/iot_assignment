#ifndef __PERSON__
#define __PERSON__

class Person {
public:

  Person();

  bool checkWantCoffee();
  bool checkCanTakeCoffee();
  bool checkMaintenance();

  void setWantCoffee(bool state);
  void setCanTakeCoffee(bool state);
  void setMaintenance(bool state);
  void decreaseCoffeeNumber();

  float getCurrentDistance();
  void setCurrentDistance(float distance);
  int getCoffeeNumber();
  void refillCoffee();
  void refillCoffee(int value);

private:
  float currentDistance;
  bool wantCoffee;
  bool canTakeCoffee;
  int numCoffee;
  bool maintenance;
};

#endif
