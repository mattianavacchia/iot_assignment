#include "LowPower.h"
#include "PersonDetectorTask.h"
#include "Sonar.h"
#include "Pir.h"
#include "config.h"
#include "MsgService.h"
#include "Led.h"

PersonDetectorTask::PersonDetectorTask(Person* p) {
  person = p;
  pProx = new Sonar(SONAR_E, SONAR_T);
  pPres = new Pir(PIR_PIN);
}

void PersonDetectorTask::init(int period) {
  Task::init(period);
  state = STAND_BY;
  //attachInterrupt(digitalPinToInterrupt(PIR_PIN),wakeUpNow, RISING);
}

void PersonDetectorTask::tick() {
  switch(state) {
    case IDLE: {
      if (!person->checkMaintenance()) {
        state = ON;
        MsgService.sendMsg(MSG_CLEAR);
      }
      break;
    }
    case STAND_BY: {
      if(pPres->isDetected()) {
        state = ON;
        arrivalTime = 0;
        awayTime = 0;
      }
      /*Serial.println("GOING TO SLEEP");
      Serial.flush();
      set_sleep_mode(SLEEP_MODE_PWR_DOWN);  
      sleep_enable();
      sleep_mode(); 
      sleep_disable();
      state = ON;
      detachInterrupt(digitalPinToInterrupt(PIR_PIN));*/
      Serial.println("GOING TO SLEEP");
      attachInterrupt(digitalPinToInterrupt(PIR_PIN),wake_routine,RISING);
      LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); // sleep until interrupt
      detachInterrupt(digitalPinToInterrupt(PIR_PIN)); //stop interrupt to allow program to run
      // All delays need to be multiplied by the ratio of crystal speed. The numbers below are for 4 MHz
      delay(25); 
      Serial.println("MOTION"); // notify through serial port
      break;
    }
    case ON: {
      if(arrivalTime < DT1 && pProx->getDistance() < DIST1) {
         arrivalTime += myPeriod;
      }
      if(pProx->getDistance() < DIST1 && arrivalTime >= DT1) {
        //person->setCurrentDistance(pProx->getDistance());
        person->setWantCoffee(true);
        state = READY;
        awayTime = 0;
        takeTime = 0;
        arrivalTime = 0;
        MsgService.sendMsg(MSG_WELCOME);
      }
      if(!pPres->isDetected() && pProx->getDistance() > DIST1) {
        awayTime += myPeriod;
        if(awayTime > DT2B) {
          Serial.println("GOING TO STANBY");
          state = STAND_BY;
          MsgService.sendMsg(MSG_CLEAR);
          person->setWantCoffee(false);
        }
      }
      if (pPres->isDetected() || pProx->getDistance() < DIST1) {
        awayTime = 0;
      }

      break;
    }
    case READY: {
      if(!person->checkMaintenance()) {
        if (awayTime < DT2A && pProx->getDistance() > DIST1) {
          awayTime += myPeriod; 
        }
        if (pProx->getDistance() > DIST1  && awayTime >= DT2A) {
          state = ON;
          awayTime = 0;
          person->setWantCoffee(false);
          MsgService.sendMsg(MSG_CLEAR);
        }
        // if persona puo prendere -> cambio strato
        if (person->checkCanTakeCoffee()) {
          state = COFFETAKE;
        }
      } else {
        state = IDLE;
      }
      break;
    }
    case COFFETAKE:{
      takeTime += myPeriod;
      if (pProx->getDistance() < DIST2 && takeTime < DT4) {
        MsgService.sendMsg(MSG_COFFEE_TAKEN);
        this->coffeTakeActions();
      }
      if (takeTime > DT4) {
        this->coffeTakeActions();
      }
      break;
    }
  }
}

void PersonDetectorTask::coffeTakeActions() {
  person->setCanTakeCoffee(false);
  person->setWantCoffee(true);
  state = READY;
  takeTime = 0;
  MsgService.sendMsg(MSG_WELCOME);
}

void PersonDetectorTask::sleep() {
}

void PersonDetectorTask::wake_routine() {
  Serial.println("WakeUp");
}
