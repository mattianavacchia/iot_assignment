#include "Task.h"
#include "Person.h"
#include "Button.h"
#include "Led.h"
#include "Booty.h"

class CoffeeMachineTask: public Task {

public:

  CoffeeMachineTask(Person* p);
  void init(int period);
  void tick();

private:

  Person* person;
  Button* buttonMake;
  Led* led[3];
  Booty* sugarBooty;

  enum { IDLE, DOCOFFEE, COFFEEREADY, MAINTENANCE } state;
};
