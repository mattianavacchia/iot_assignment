#include "Person.h"
#include "config.h"

Person::Person() {
  wantCoffee = false;
  canTakeCoffee = false;
  numCoffee = NUM_COFFEE;
  maintenance = false;
}

bool Person::checkWantCoffee() {
  return wantCoffee;
}

bool Person::checkCanTakeCoffee() {
  return canTakeCoffee;
}

bool Person::checkMaintenance() {
  return maintenance;
}

void Person::setWantCoffee(bool state) {
  wantCoffee = state;
}

void Person::setCanTakeCoffee(bool state) {
  canTakeCoffee = state;
}

void Person::setMaintenance(bool state) {
  maintenance = state;
}

void Person::decreaseCoffeeNumber() {
  numCoffee--;
}

float Person::getCurrentDistance() {
  return currentDistance;
}

void Person::setCurrentDistance(float distance) {
  currentDistance = distance;
}

int Person::getCoffeeNumber() {
  return numCoffee;
}

void Person::refillCoffee() {
  numCoffee = NUM_COFFEE;
}

void Person::refillCoffee(int value) {
  numCoffee = value;
}
