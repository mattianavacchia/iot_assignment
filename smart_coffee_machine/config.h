#ifndef __CONFIG__
#define __CONFIG__

#define LED_BLUE 11
#define LED_YELLOW 12
#define LED_GREEN 13

#define BUTTON_MAKE 7
#define POT_SUGAR A0

#define PIR_PIN 2
#define SONAR_T 9
#define SONAR_E 8

#define DIST1 0.3
#define DIST2 0.1

#define DT1 1000
#define DT2A 5000
#define DT2B 5000
#define DT3 3000
#define DT4 5000

#define NUM_COFFEE 2

#define MSG_CLEAR          "0"
#define MSG_WELCOME        "1"
#define MSG_MAKING_COFFEE  "2"
#define MSG_COFFEE_READY   "3"
#define MSG_COFFEE_TAKEN   "4"
#define MSG_NO_COFFEE      "5"
#define MSG_REFILL_COFFEE  "6|0"
#define MSG_REFILL_DONE    "6|1"
#define MSG_SUGAR          "7|"

#endif
