/*
 * SEIOT a.a. 2018/2019
 *
 * CONSEGNA #2 - SMART COFFEE MACHINE
 *
 * Hnatyshyn Nazar, Navacchia Mattia
 */
#include "Scheduler.h"
#include "Person.h"
#include "PersonDetectorTask.h"
#include "CoffeeMachineTask.h"

/* Variables*/
Scheduler scheduler;

void setup() {
  Serial.begin(9600);
  scheduler.init(50);

  //MsgService.init();

  Person* person = new Person();

  PersonDetectorTask* personDetector = new PersonDetectorTask(person);
  personDetector->init(50);
  scheduler.addTask(personDetector);

  CoffeeMachineTask* CoffeeMachine = new CoffeeMachineTask(person);
  CoffeeMachine->init(50);
  scheduler.addTask(CoffeeMachine);
}

void loop() {
  scheduler.schedule();
}
