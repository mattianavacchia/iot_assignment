#include "BootyImpl.h"
#include "Arduino.h"
#include "config.h"

BootyImpl::BootyImpl(int pin) {
  this->pin = pin;
  pinMode(this->pin, INPUT);
  this->value = -1;
}

int BootyImpl::getValue() {
  int newValue = analogRead(POT_SUGAR);
  if (newValue != this->value) {
    this->value = map(newValue, 0, 1020, 0, 5);
  }

  return this->value;
}

bool BootyImpl::isNewValue() {
  return this->value != map(analogRead(POT_SUGAR), 0, 1020, 0, 5);
}

void BootyImpl::resetNewValue() {
  this->value = -1;
}
