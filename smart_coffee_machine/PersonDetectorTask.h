#include "Task.h"
#include "ProximitySensor.h"
#include "PresenceSensor.h"
#include "Person.h"

class PersonDetectorTask: public Task {

public:

  PersonDetectorTask(Person* p);
  void init(int period);
  void tick();
  void sleep();
  static void wake_routine();

private:

  void coffeTakeActions();

  Person* person;
  ProximitySensor* pProx;
  PresenceSensor* pPres;

  int arrivalTime;
  int presenceTime;
  int awayTime;
  int takeTime;

  enum { IDLE, STAND_BY, ON, READY, COFFETAKE } state;
};
