package smart_coffee_machine;

import com.sun.javafx.application.PlatformImpl;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        PlatformImpl.startup(() -> {
        });
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("view.fxml"));
    	ViewController viewController = new ViewController();
    	loader.setController(viewController);
    	Platform.runLater(() -> {
    	    try {
                primaryStage.setTitle("Smart Coffee Machine");
                primaryStage.setScene(new Scene(loader.load()));
                primaryStage.show();
                primaryStage.setOnCloseRequest(e -> System.exit(0));
    	    } catch (Exception e) {
    	        System.out.println("Unable to load graphic environment.");
                e.printStackTrace();
            }
    	});
    }


    public static void main(String[] args) {
        launch(args);
    }
}
