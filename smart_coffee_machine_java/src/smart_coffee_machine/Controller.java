package smart_coffee_machine;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import gnu.io.CommPortIdentifier;
import smart_coffee_machine.rxtx.SerialCommChannel;

public class Controller {
	
	private ViewController viewController;
	private String selectedPort = "";
	private MonitoringThread monitoringThread;
	private SerialCommChannel channel;
	
	public Controller() {
	}
	
	public void addUI(final ViewController viewController) {
		this.viewController = viewController;
	}
	
	public List<String> getPorts() {
		List<String> list = new ArrayList<>();
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
			list.add(currPortId.getName());
		}
		return list;
	}
	
	public void setSelectedPort(final String port) {
		this.selectedPort = port;
	}
	
	public void connect() throws Exception {
		channel = new SerialCommChannel(selectedPort, 9600);
		this.monitoringThread = new MonitoringThread(channel, this.viewController);
		this.monitoringThread.start();
	}
	
	public void refillCoffee() {
	    this.channel.sendMsg(MonitoringThread.MSG_REFILL_COFFEE);
	    System.out.println(MonitoringThread.MSG_REFILL_COFFEE);
	}
	
	public void sendMsg(final String msg) {
	    this.channel.sendMsg(msg);
	}
	
}
