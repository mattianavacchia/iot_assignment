package smart_coffee_machine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.animation.PauseTransition;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Duration;

public class ViewController {
	
	private Controller controller;
	
	@FXML
	private ComboBox<String> cmbChannel;
	
	@FXML
	private Text txtMessage, txtMaintenance;
	
	@FXML
	private Button btnSugar1, btnSugar2, btnSugar3, btnSugar4, btnSugar5, btnFill, btnConnect;
	
	@FXML
	private VBox container;
	
	@FXML
	private HBox sugarContainer;

	private List<Button> buttonsSugar;
	
	// Add a public no-args constructor
	public ViewController() {
		this.controller = new Controller();
		this.controller.addUI(this);
	}
	
	@FXML
	private void initialize() {
		updateComPort();
		this.buttonsSugar = new ArrayList<>();
		this.buttonsSugar.addAll(Arrays.asList(btnSugar1, btnSugar2, btnSugar3, btnSugar4, btnSugar5));
		this.buttonsSugar.forEach(e -> {
		    e.setStyle("-fx-background-color: GRAY;");
		});
		this.hideSugarBox();
	}
	
	@FXML
	private void cmbChannelClicked() {
	    updateComPort();
	}
	
	@FXML
	private void connect_Clicked() {
	    this.controller.setSelectedPort(this.cmbChannel.getValue());
	    try {
	        this.controller.connect();
	        this.btnConnect.setDisable(true);
	        this.cmbChannel.setDisable(true);
	    } catch (Exception e) {
                // TODO: handle exception
            }
	    
	}
	
	private void updateComPort() {
	    this.cmbChannel.setItems(FXCollections.observableArrayList(controller.getPorts()));
	    if ( this.cmbChannel.getItems().size() > 0 ) {
	        this.btnConnect.setDisable(false);
	        if (this.cmbChannel.getValue() == null ) {
	            this.cmbChannel.setValue(this.cmbChannel.getItems().get(0));
	        }
	    } else {
	        this.btnConnect.setDisable(true);
	    }
	}
	
	public void setMessage(final String msg) {
	    this.txtMessage.setText(msg);
	    
	    if (!this.txtMaintenance.getText().isEmpty()) {
	        this.txtMaintenance.setText("");
	    }
	}
	
	public void enableRefill(final String msg) {
	    this.txtMaintenance.setText(msg);
	    this.btnFill.setDisable(false);
	}
	
	@FXML
	public void coffeeRefill_Clicked() {
	    this.controller.refillCoffee();
	}
	
	public void disableRefill(final String msg) {
	    this.txtMaintenance.setText(msg);
	    this.btnFill.setDisable(true);
	}
	
	public void updateSugar(final int value) {
	    for (int i = 0; i < this.buttonsSugar.size(); i++) {
        if (i < value) {
               this.buttonsSugar.get(i).setStyle("-fx-background-color: GREEN;");
           } else {
               this.buttonsSugar.get(i).setStyle("-fx-background-color: GRAY;");
           }
        }
	}
	
	public void hideSugarBox() {
	    this.buttonsSugar.forEach(e -> {
            e.setStyle("-fx-background-color: GRAY;");
        });
	    this.sugarContainer.setVisible(false);
	}
	
	public void showSugarBox() {
	    this.sugarContainer.setVisible(true);
	}
}
