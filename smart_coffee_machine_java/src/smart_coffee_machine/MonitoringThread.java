package smart_coffee_machine;

import java.util.HashMap;
import java.util.Map;

import smart_coffee_machine.rxtx.SerialCommChannel;

public class MonitoringThread extends Thread {
    private SerialCommChannel channel;
    private ViewController view;
    //LogView logger;
    private boolean running;
    
    private Map<String, String> MSG_MAP = new HashMap<>();
    
    static final String MSG_CLEAR          = "0";
    static final String MSG_WELCOME        = "1";
    static final String MSG_MAKING_COFFEE  = "2";
    static final String MSG_COFFEE_READY   = "3";
    static final String MSG_COFFEE_TAKEN   = "4";
    static final String MSG_NO_COFFEE      = "5";
    static final String MSG_REFILL_COFFEE  = "6|0";
    static final String MSG_REFILL_DONE    = "6|1";
    static final String MSG_SUGAR          = "7|";
    
    public MonitoringThread(SerialCommChannel channel, ViewController view) throws Exception {
        this.view = view;
        //this.logger = log;
        this.channel = channel;
        MSG_MAP.put(MSG_CLEAR, "");
        MSG_MAP.put(MSG_WELCOME, "Welcome!");
        MSG_MAP.put(MSG_MAKING_COFFEE, "Making a coffee");
        MSG_MAP.put(MSG_COFFEE_READY, "The coffee is ready");
        MSG_MAP.put(MSG_COFFEE_TAKEN, "");
        MSG_MAP.put(MSG_NO_COFFEE, "No more coffee Waiting for recharge");
        MSG_MAP.put(MSG_REFILL_DONE, "Coffe refilled: ");
    }
    
    public void run(){
        System.out.println("Waiting Arduino for rebooting...");     
        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Ready.");       
        this.running = true;
        while (this.running){
            try {
                String msg = channel.receiveMsg();
                System.out.println(msg);

                if (msg.equals(MSG_CLEAR)) {
                    this.view.setMessage(MSG_MAP.get(msg));
                    this.view.hideSugarBox();
                } else if (msg.equals(MSG_WELCOME)|| msg.equals(MSG_MAKING_COFFEE) || msg.equals(MSG_COFFEE_READY) || msg.equals(MSG_COFFEE_TAKEN)) {
                    this.view.setMessage(MSG_MAP.get(msg));
                    this.view.showSugarBox();
                } else if (msg.equals(MSG_NO_COFFEE)) {
                    this.view.enableRefill(MSG_MAP.get(MSG_NO_COFFEE));
                } else if (msg.startsWith(MSG_REFILL_DONE)) {
                    String num_coffee = msg.substring(MSG_REFILL_DONE.length());
                    this.view.disableRefill(MSG_MAP.get(MSG_REFILL_DONE) + " " + num_coffee);
                } else if (msg.startsWith(MSG_SUGAR)) {
                    String value = msg.substring(MSG_SUGAR.length());
                    this.view.updateSugar(Integer.parseInt(value));
                }
                
            } catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
    
    public void stopRunning() {
        this.running = false;
    }
}
